import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
const deptData = require('../assets/initial-dept-data.json');
const pplData = require('../assets/initial-people-data.json');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    if (localStorage.getItem(environment.departmentStorageKey) === null) {
      localStorage.setItem(environment.departmentStorageKey, JSON.stringify(deptData));
    }
    if (localStorage.getItem(environment.peopleStorageKey) === null) {
      localStorage.setItem(environment.peopleStorageKey, JSON.stringify(pplData));
    }
  }
}
