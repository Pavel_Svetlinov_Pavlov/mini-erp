import { Injectable } from '@angular/core';
import {
  HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Person } from '../../models/person';
import { Department } from '../../models/department';
import { environment } from 'src/environments/environment';

let peopleList: any[] = JSON.parse(localStorage.getItem(environment.peopleStorageKey)!) || [];
let departmentList: any[] = JSON.parse(localStorage.getItem(environment.departmentStorageKey)!) || [];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url, method, headers, body } = request;

    return handleRoute();

    function handleRoute() {
      switch (true) {
        case url.endsWith('/people') && method === 'POST':
          return createPerson();
        case url.endsWith('/people') && method === 'GET':
          return getPeopleList();
        case url.match(/\/people\/\d+$/) && method === 'GET':
          return getPersonById();
        case url.match(/\/people\/\d+$/) && method === 'PUT':
          return editPerson();
        case url.match(/\/people\/\d+$/) && method === 'DELETE':
          return deletePerson();

        case url.endsWith('/department') && method === 'POST':
          return createDepartment();
        case url.endsWith('/department') && method === 'GET':
          return getDepartmentsList();
        case url.match(/\/department\/\d+$/) && method === 'GET':
          return getDepartmentById();
        case url.match(/\/department\/\d+$/) && method === 'PUT':
          return editDepartment();
        case url.match(/\/department\/\d+$/) && method === 'DELETE':
          return deleteDepartment();

        default:
          // pass through any requests not handled above
          return next.handle(request);
      }
    }

    // people route functions
    function createPerson() {
      const person = body;
      peopleList = updateListFromStorage(environment.peopleStorageKey);
      person.personId = peopleList.length ? Math.max.apply(Math, peopleList.map((e) => e.personId)) + 1 : 1;
      peopleList.push(person);
      localStorage.setItem(environment.peopleStorageKey, JSON.stringify(peopleList));
      return ok();
    }

    function getPeopleList() {
      return ok(peopleList.map(x => personBasicDetails(x)));
    }

    function getPersonById() {
      const person = peopleList.find(x => x.personId === idFromUrl());
      return ok(personBasicDetails(person));
    }

    function editPerson() {
      const person = peopleList.find(x => x.personId === idFromUrl());
      Object.assign(person, body);
      localStorage.setItem(environment.peopleStorageKey, JSON.stringify(peopleList));
      return ok();
    }

    function deletePerson() {
      const personId = idFromUrl();
      peopleList = peopleList.filter(x => x.personId != personId);
      localStorage.setItem(environment.peopleStorageKey, JSON.stringify(peopleList));
      return ok();
    }

    // departments route functions
    function createDepartment() {
      const department = body;
      departmentList = updateListFromStorage(environment.departmentStorageKey);
      department.departmentId = departmentList.length ? Math.max.apply(Math, departmentList.map((e) => e.departmentId)) + 1 : 1;
      departmentList.push(department);
      localStorage.setItem(environment.departmentStorageKey, JSON.stringify(departmentList));
      return ok();
    }

    function getDepartmentsList() {
      return ok(departmentList.map(x => departmentBasicDetails(x)));
    }

    function getDepartmentById() {
      const department = departmentList.find(x => x.departmentId === idFromUrl());
      return ok(departmentBasicDetails(department));
    }

    function editDepartment() {
      const department = departmentList.find(x => x.departmentId === idFromUrl());
      Object.assign(department, body);
      localStorage.setItem(environment.departmentStorageKey, JSON.stringify(departmentList));
      return ok();
    }

    function deleteDepartment() {
      const departmentId = idFromUrl();
      const departmentName = departmentList.filter(x => x.departmentId == departmentId)[0].departmentName;
      departmentList = departmentList.filter(x => x.departmentId != departmentId);
      peopleList.map(x => {
        if (x.department == departmentName) {
          x.department = null;
        }
      });
      localStorage.setItem(environment.departmentStorageKey, JSON.stringify(departmentList));
      localStorage.setItem(environment.peopleStorageKey, JSON.stringify(peopleList));
      return ok();
    }

    // helper functions
    function ok(body?: any) {
      return of(new HttpResponse({ status: 200, body }))
        .pipe(delay(500)); // delay observable to simulate server api call
    }

    function personBasicDetails(person: Person) {
      const { personId, fullName, birthDate, department } = person;
      return { personId, fullName, birthDate, department };
    }

    function departmentBasicDetails(department: Department) {
      const { departmentId, departmentName } = department;
      return { departmentId, departmentName };
    }

    function idFromUrl() {
      const urlParts = url.split('/');
      return parseInt(urlParts[urlParts.length - 1]);
    }

    function updateListFromStorage(key: string) {
      return JSON.parse(localStorage.getItem(key)!) || [];
    }
  }
}

export const fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
