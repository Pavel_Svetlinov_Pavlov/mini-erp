import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs';
import {Department} from '../../models/department';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService {
  public departmentEditFormSubject$ = new BehaviorSubject<string>('');
  public getDepartmentsListSubject$ = new BehaviorSubject<Department[]>([]);

  constructor(private http: HttpClient) {
  }

  createDepartment(department: Department) {
    return this.http
      .post(
        `${environment.apiUrl}/department`,
        department
      )
  }

  deleteDepartment(id: string) {
    return this.http.delete(`${environment.apiUrl}/department/${id}`);
  }

  editDepartment(department: Department, id: string) {
    return this.http.put(`${environment.apiUrl}/department/${id}`, department);
  }

  getDepartmentById(id: string) {
    return this.http.get<Department>(`${environment.apiUrl}/department/${id}`);
  }

  getAllDepartments() {
    return this.http.get<Department[]>(`${environment.apiUrl}/department`);
  }

  fetchDepartmentsData(isAscending?: boolean): void {
    this.http.get<Department[]>(`${environment.apiUrl}/department`)
      .subscribe(response => {
        isAscending ? response = response.sort() : response = response.reverse();
        this.getDepartmentsListSubject$.next(response);
      });
  }
}
