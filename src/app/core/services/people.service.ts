import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs';
import {Person} from '../../models/person';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {
  public personEditFormSubject$ = new BehaviorSubject<string>('');
  public getPeopleListSubject$ = new BehaviorSubject<Person[]>([]);

  constructor(private http: HttpClient) {
  }

  createPerson(person: Person) {
    return this.http
      .post(
        `${environment.apiUrl}/people`,
        person
      )
  }

  deletePerson(id: string) {
    return this.http
      .delete(`${environment.apiUrl}/people/${id}`);
  }

  editPerson(person: Person, id: string) {
    return this.http
      .put(`${environment.apiUrl}/people/${id}`, person);
  }

  getPersonById(id: string) {
    return this.http.get<Person>(`${environment.apiUrl}/people/${id}`);
  }

  getAllPeople() {
    return this.http.get<Person[]>(`${environment.apiUrl}/people`);
  }

  fetchPeopleData(isAscending?: boolean): void {
    this.http.get<Person[]>(`${environment.apiUrl}/people`)
      .subscribe((response) => {
        isAscending ? response = response.sort() : response = response.reverse();
        this.getPeopleListSubject$.next(response);
      });
  }
}
