export class Person {
  personId?: string;
  fullName?: string;
  birthDate?: string;
  department?: string;

  constructor() {
  }
}
