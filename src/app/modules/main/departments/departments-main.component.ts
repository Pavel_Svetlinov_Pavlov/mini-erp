import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {DepartmentsService} from '../../../core/services/departments.service';

@Component({
  selector: 'app-departments-main',
  templateUrl: './departments-main.component.html',
  styleUrls: ['./departments-main.component.scss']
})
export class DepartmentsMainComponent implements OnInit {
  renderFormSubject: Subject<string> | undefined;

  constructor(private departmentService: DepartmentsService) {
  }

  ngOnInit(): void {
    this.renderFormSubject = this.departmentService.departmentEditFormSubject$;
    this.renderFormSubject.next('');
  }
}
