import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { SidebarDepartmentsComponent } from './sidebar-departments/sidebar-departments.component';
import { DetailsDepartmentComponent } from './details-department/details-department.component';
import {PeopleModule} from '../people/people.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
    declarations: [

        SidebarDepartmentsComponent,
        DetailsDepartmentComponent
    ],
  exports: [
    DetailsDepartmentComponent,
    SidebarDepartmentsComponent
  ],
  imports: [
    CommonModule,
    PeopleModule,
    ReactiveFormsModule
  ]
})
export class DepartmentsModule {
}
