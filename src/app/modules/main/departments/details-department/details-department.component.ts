import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DepartmentsService} from '../../../../core/services/departments.service';
import {PeopleService} from '../../../../core/services/people.service';
import {Department} from '../../../../models/department';
import {Person} from '../../../../models/person';

@Component({
  selector: 'app-details-department',
  templateUrl: './details-department.component.html',
  styleUrls: ['./details-department.component.scss']
})
export class DetailsDepartmentComponent implements OnInit {
  public isEditMode: boolean = false;
  public isFormInvalid: boolean = false;
  public departmentForm!: FormGroup;
  public department: Department = new Department();
  public dropdownList: Person[] = [];
  public selectedPeople: Person[] = [];

  constructor(private departmentsService: DepartmentsService, private peopleService: PeopleService) {
  }

  ngOnInit(): void {
    this.departmentsService.departmentEditFormSubject$.subscribe(departmentId => {
      if (departmentId !== 'Create' && departmentId != '') {
        this.loadEditForm(departmentId);
      } else {
        this.loadCreateForm();
      }
    });

    // create empty form with validations
    this.departmentForm = new FormGroup({
      'departmentId': new FormControl(null),
      'departmentName': new FormControl(
        null,
        [Validators.required,
          Validators.pattern('^(?=.*[A-Za-z\\s])[A-Za-z\\s\\W]{5,}$')]),
      'people': new FormControl(this.getPeopleForOptions())
    });
  }

  onSubmit() {
    if (this.departmentForm.valid) {
      // collect data from the form
      this.department = {
        departmentId: this.departmentForm.value.departmentId,
        departmentName: this.departmentForm.value.departmentName,
      }
      if (this.isEditMode && this.department.departmentId) {
        this.departmentsService.editDepartment(this.department, this.department.departmentId).subscribe();
        this.updatePeopleDepartment(this.department, this.selectedPeople);
      } else {
        this.departmentsService.createDepartment(this.department).subscribe();
        this.updatePeopleDepartment(this.department, this.selectedPeople);
      }
      //refresh data
      this.departmentsService.fetchDepartmentsData();
      this.departmentsService.departmentEditFormSubject$.next('');
    } else {
      // handle form validation errors
      this.isFormInvalid = true;
    }
  }

  getPeopleForOptions() {
    this.peopleService.getAllPeople().subscribe(data => {
      this.dropdownList = data;
    });
  }

  onChange(event: any) {
    let selectedPersonId = event.target.value;
    for (let i = 0; i < this.dropdownList.length; i++) {
      if (selectedPersonId == this.dropdownList[i].personId) {
        let poppedElement = this.dropdownList.splice(i, 1);
        this.selectedPeople.push(poppedElement[0]);
      }
    }
  }

  updatePeopleDepartment(department: Department, listOfPeople: Person[]) {
    for (let el of listOfPeople) {
      el.department = department.departmentName;
      if (el.personId != undefined) {
        this.peopleService.editPerson(el, el.personId).subscribe();
      }
    }
  }

  private loadEditForm(id: string) {
    this.isEditMode = true;
    this.departmentsService.getDepartmentById(id).subscribe(department => {
      this.departmentForm.patchValue({
        'departmentId': department.departmentId,
        'departmentName': department.departmentName,
      });
      this.peopleService.getAllPeople().subscribe(response => {
        this.dropdownList = response.filter(x => x.department != department.departmentName);
        this.selectedPeople = response.filter(x => x.department == department.departmentName);
      });
    });
  }

  private loadCreateForm() {
    this.isEditMode = false;
    this.departmentForm?.reset();
    this.selectedPeople = [];
  }
}
