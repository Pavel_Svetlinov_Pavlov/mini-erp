import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Department } from '../../../../models/department';
import { DepartmentsService } from '../../../../core/services/departments.service';

@Component({
  selector: 'app-sidebar-departments',
  templateUrl: './sidebar-departments.component.html',
  styleUrls: ['./sidebar-departments.component.scss']
})
export class SidebarDepartmentsComponent implements OnInit {

  public departmentsList$: Observable<Department[]> | undefined;
  public selectedIndex: number | undefined;
  public searchText: string = '';
  public isAscending: boolean = false;

  constructor(private departmentsService: DepartmentsService) {
  }

  ngOnInit(): void {
    this.departmentsService.fetchDepartmentsData();
    this.departmentsList$ = this.departmentsService.getDepartmentsListSubject$;
  }

  deleteDepartment(id: string | undefined): void {
    if (window.confirm('Are sure you want to delete?')) {
      if (id) {
        this.departmentsService.deleteDepartment(id).subscribe(() => {
          this.departmentsService.fetchDepartmentsData();
        })
      }
      this.departmentsService.departmentEditFormSubject$.next('');
    }
  }

  editDepartment(id: string | undefined) {
    if (id) {
      this.departmentsService.departmentEditFormSubject$.next(id);
    }
  }

  renderCreateDepartmentForm() {
    this.departmentsService.departmentEditFormSubject$.next('Create');

  }

  setRow(index: number) {
    this.selectedIndex = index;
  }

  onSearchTextEntered(searchValue: string) {
    this.searchText = searchValue;
  }

  onSortClick() {
    this.isAscending = this.isAscending ? false : true;
    this.departmentsService.fetchDepartmentsData(this.isAscending);
  }
}
