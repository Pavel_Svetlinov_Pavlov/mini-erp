import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MainRoutingModule} from './main.routing.module';
import {PeopleMainComponent} from './people/people-main.component';
import {PeopleModule} from './people/people.module';
import {DepartmentsMainComponent} from './departments/departments-main.component';
import {DepartmentsModule} from './departments/departments.module';

@NgModule({
  declarations: [
    DepartmentsMainComponent,
    PeopleMainComponent
  ],
  exports: [
    DepartmentsMainComponent,
    PeopleMainComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MainRoutingModule,
    PeopleModule,
    DepartmentsModule
  ]
})

export class MainModule {
}
