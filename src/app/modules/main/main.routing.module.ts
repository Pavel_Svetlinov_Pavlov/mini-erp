import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PeopleMainComponent} from './people/people-main.component';
import {DepartmentsMainComponent} from './departments/departments-main.component';

const routes: Routes = [
  {
    path: 'people',
    component: PeopleMainComponent
  },
  {
    path: 'departments',
    component: DepartmentsMainComponent
  }
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
