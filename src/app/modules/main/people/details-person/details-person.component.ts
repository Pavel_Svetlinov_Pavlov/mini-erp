import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DepartmentsService} from '../../../../core/services/departments.service';
import {PeopleService} from '../../../../core/services/people.service';
import {Department} from '../../../../models/department';
import {Person} from '../../../../models/person';

@Component({
  selector: 'app-details-person',
  templateUrl: './details-person.component.html',
  styleUrls: ['./details-person.component.scss']
})
export class DetailsPersonComponent implements OnInit {
  public isEditMode: boolean = false;
  public isFormInvalid: boolean = false;
  public employeeForm!: FormGroup;
  public employee: Person = new Person();
  public dropdownList: Department[] = [];

  constructor(private peopleService: PeopleService, private departmentService: DepartmentsService) {
  }

  ngOnInit(): void {
    this.peopleService.personEditFormSubject$.subscribe(personId => {
      if (personId !== 'Create' && personId != '') {
        this.loadEditForm(personId);
      } else {
        this.loadCreateForm();
      }
    });

    // create empty form with validations
    this.employeeForm = new FormGroup({
      'personId': new FormControl(null),
      'fullName': new FormControl(
        null,
        [Validators.required, Validators.pattern('^[A-Za-z\\s]+$')]),
      'birthDate': new FormControl(
        null,
        [Validators.required, this.dateOfBirthValidator]),
      'department': new FormControl(
        this.getDepartmentsForOptions(),
        Validators.required)
    });
  }

  onSubmit(): void {
    if (this.employeeForm.valid) {
      // collect data from the form
      this.employee = {
        personId: this.employeeForm.value.personId,
        fullName: this.employeeForm.value.fullName,
        birthDate: this.employeeForm.value.birthDate,
        department: this.employeeForm.value.department,
      }
      if (this.isEditMode && this.employee.personId) {
        this.peopleService.editPerson(this.employee, this.employee.personId).subscribe();
      } else {
        this.peopleService.createPerson(this.employee).subscribe();
      }
      //refresh data
      this.peopleService.fetchPeopleData();
      this.peopleService.personEditFormSubject$.next('');
    } else {
      // handle form validation errors
      this.isFormInvalid = true;
    }
  }

  dateOfBirthValidator(control: FormControl): { [key: string]: any } | null {
    const inputDate = new Date(control.value);
    const currentYear = new Date().getFullYear();

    if (
      isNaN(inputDate.getTime()) ||
      inputDate.getFullYear() < 1900 ||
      inputDate.getFullYear() > currentYear) {
      return {'invalidDate': true};
    }
    return null;
  }

  private loadEditForm(personId: string) {
    this.isEditMode = true;
    this.peopleService.getPersonById(personId).subscribe(employee => {
      this.employeeForm.patchValue({
        'personId': employee.personId,
        'fullName': employee.fullName,
        'birthDate': employee.birthDate,
        'department': employee.department,
      });
    });
  }

  private loadCreateForm() {
    this.isEditMode = false;
    this.employeeForm?.reset();
  }

  private getDepartmentsForOptions() {
    this.departmentService.getAllDepartments().subscribe(data => {
      this.dropdownList = data;
    });
  }
}
