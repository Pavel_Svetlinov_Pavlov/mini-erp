import {Component, OnInit} from '@angular/core';
import {PeopleService} from '../../../core/services/people.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-people-main',
  templateUrl: './people-main.component.html',
  styleUrls: ['./people-main.component.scss']
})
export class PeopleMainComponent implements OnInit {
  renderFormSubject: Subject<string> | undefined;

  constructor(private peopleService: PeopleService) {
  }

  ngOnInit(): void {
    this.renderFormSubject = this.peopleService.personEditFormSubject$;
    this.renderFormSubject.next('');
  }
}

