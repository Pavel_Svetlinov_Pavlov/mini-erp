import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SidebarPeopleComponent} from './sidebar-people/sidebar-people.component';
import {DetailsPersonComponent} from './details-person/details-person.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SearchComponent} from '../../shared/search/search.component';
import {SortComponent} from '../../shared/sort/sort.component';

@NgModule({
  declarations: [
    DetailsPersonComponent,
    SidebarPeopleComponent,
    SearchComponent,
    SortComponent
  ],
  exports: [
    SidebarPeopleComponent,
    DetailsPersonComponent,
    SearchComponent,
    SortComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})

export class PeopleModule {
}
