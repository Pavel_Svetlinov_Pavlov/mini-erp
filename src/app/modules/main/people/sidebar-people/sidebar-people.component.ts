import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PeopleService } from '../../../../core/services/people.service';
import { Person } from '../../../../models/person';

@Component({
  selector: 'app-sidebar-people',
  templateUrl: 'sidebar-people.component.html',
  styleUrls: ['./sidebar-people.component.scss']
})
export class SidebarPeopleComponent implements OnInit {

  public peopleList$: Observable<Person[]> | undefined;
  public selectedIndex: number | undefined;
  public searchText: string = '';
  public isAscending: boolean = false;

  constructor(private peopleService: PeopleService) {
  }

  ngOnInit(): void {
    this.peopleService.fetchPeopleData();
    this.peopleList$ = this.peopleService.getPeopleListSubject$;
  }

  deletePerson(id: string | undefined): void {
    if (window.confirm('Are sure you want to delete?')) {
      if (id) {
        this.peopleService.deletePerson(id).subscribe(() => {
          this.peopleService.fetchPeopleData();
        })
      }
      this.peopleService.personEditFormSubject$.next('');
    }
  }

  editPerson(id: string | undefined): void {
    if (id) {
      this.peopleService.personEditFormSubject$.next(id);
    }
  }

  renderCreatePersonForm(): void {
    this.peopleService.personEditFormSubject$.next('Create');
  }

  setRow(index: number) {
    this.selectedIndex = index;
  }

  onSearchTextEntered(searchValue: string) {
    this.searchText = searchValue;
  }

  onSortClick() {
    this.isAscending = this.isAscending ? false : true;
    this.peopleService.fetchPeopleData(this.isAscending);
  }
}
